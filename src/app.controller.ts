import { User } from './users/user.entity';
import { AuthService } from './auth/auth.service';
import { UsersService } from './users/users.service';
import { Controller, Post, Body, UseGuards, Get, Req } from '@nestjs/common';
import { JwtAuthGuard } from './auth/jwt-auth.guard';

@Controller()
export class AppController {
  constructor(
    private authService: AuthService,
    private usersService: UsersService,
  ) {}

  @Post('login')
  async login(@Body() user: User) {
    return this.authService.login(user);
  }

  @Post('register')
  async register(@Body() user: User) {
    return this.usersService.create(user);
  }

  @UseGuards(JwtAuthGuard)
  @Get('profile')
  getProfil(@Req() req) {
    return req.user;
  }
}
