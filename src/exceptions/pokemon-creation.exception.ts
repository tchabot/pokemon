import { HttpException, HttpStatus } from '@nestjs/common';

export class PokemonCreationException extends HttpException {
  constructor(message: string) {
    super(message, HttpStatus.NOT_ACCEPTABLE);
  }
}
