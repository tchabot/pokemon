import { AppController } from './app.controller';
import { BoxModule } from './boxes/boxes.module';
import { PokemonTypeModule } from './pokemon-types/pokemon-types.module';
import { PokemonModule } from './pokemons/pokemons.module';
import { Module } from '@nestjs/common';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Connection } from 'typeorm';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: 'localhost',
      port: 5432,
      username: 'postgres',
      password: '',
      database: 'pokemon',
      entities: ['dist/**/*.entity{.ts,.js}'],
      autoLoadEntities: true,
      synchronize: true
    }),
    PokemonModule,
    PokemonTypeModule,
    UsersModule,
    BoxModule,
    AuthModule,
    UsersModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  private connection: Connection;
}
