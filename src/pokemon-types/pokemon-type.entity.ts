import { Pokemon } from './../pokemons/pokemon.entity';
import { PrimaryGeneratedColumn, Entity, Column, OneToMany } from 'typeorm';

@Entity()
export class PokemonType {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @OneToMany(
    type => Pokemon,
    pokemon => pokemon.pokemonType,
  )
  pokemons: Pokemon;
}
