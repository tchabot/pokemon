import { UpdatePokemonTypeDto } from './dto/update-pokemon-type.dto';
import { CreatePokemonTypeDto } from './dto/create-pokemon-type.dto';
import { PokemonType } from './pokemon-type.entity';
import { PokemonTypeController } from './pokemon-types.controller';
import { PokemonTypeService } from './pokemon-types.service';
import { Repository } from 'typeorm';
import { Test } from '@nestjs/testing';

describe('PokemonTypeController', () => {
  let pokemonTypeController: PokemonTypeController;
  let pokemonTypeService: PokemonTypeService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [PokemonTypeController],
      providers: [
        {
          provide: 'PokemonTypeRepository',
          useClass: Repository,
        },
        PokemonTypeService,
      ],
    }).compile();

    pokemonTypeService = moduleRef.get<PokemonTypeService>(PokemonTypeService);
    pokemonTypeController = moduleRef.get<PokemonTypeController>(
      PokemonTypeController,
    );
  });

  describe('findAll', () => {
    it('should return an array of PokemonTypes', async () => {
      const result = [];

      jest.spyOn(pokemonTypeService, 'findAll').mockResolvedValue(result);
      expect(await pokemonTypeController.findAll()).toBe(result);
    });
  });

  describe('findOne', () => {
    it('should return a PokemonType', async () => {
      const result = new PokemonType();

      jest.spyOn(pokemonTypeService, 'findOne').mockResolvedValue(result);
      expect(await pokemonTypeController.findOne(1)).toBe(result);
    });
  });

  describe('create', () => {
    it('should return a new PokemonType', async () => {
      const pokemonType = new PokemonType();
      const pokemonTypeDto = new CreatePokemonTypeDto();

      jest.spyOn(pokemonTypeService, 'create').mockResolvedValue(pokemonType);
      expect(await pokemonTypeController.create(pokemonTypeDto)).toBe(
        pokemonType,
      );
    });
  });

  describe('update', () => {
    it('should return an updated PokemonType', async () => {
      const pokemonType = new PokemonType();
      const updatePokemonTypeDto = new UpdatePokemonTypeDto();

      jest.spyOn(pokemonTypeService, 'update').mockResolvedValue(pokemonType);
      expect(
        await pokemonTypeController.update(
          pokemonType.id,
          updatePokemonTypeDto,
        ),
      ).toBe(pokemonType);
    });
  });

  describe('delete', () => {
    it('should delete a PokemonType', async () => {
      const pokemonType = new PokemonType();

      jest.spyOn(pokemonTypeService, 'remove').mockResolvedValue();
      expect(await pokemonTypeController.delete(pokemonType.id)).toBe(
        undefined,
      );
    });
  });
});
