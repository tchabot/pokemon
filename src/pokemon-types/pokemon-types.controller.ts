import { JwtAuthGuard } from './../auth/jwt-auth.guard';
import { UpdatePokemonTypeDto } from './dto/update-pokemon-type.dto';
import { CreatePokemonTypeDto } from './dto/create-pokemon-type.dto';
import { PokemonType } from './pokemon-type.entity';
import { PokemonTypeService } from './pokemon-types.service';
import {
  Controller,
  Get,
  Post,
  Delete,
  Body,
  Put,
  Param,
  UseGuards,
} from '@nestjs/common';
@UseGuards(JwtAuthGuard)
@Controller('pokemon-types')
export class PokemonTypeController {
  constructor(private readonly pokemonTypeService: PokemonTypeService) {}

  @Get()
  findAll(): Promise<PokemonType[]> {
    return this.pokemonTypeService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: number): Promise<PokemonType> {
    return this.pokemonTypeService.findOne(id);
  }

  @Post()
  create(
    @Body() createPokemonTypeDto: CreatePokemonTypeDto,
  ): Promise<PokemonType> {
    return this.pokemonTypeService.create(createPokemonTypeDto);
  }

  @Put(':id')
  update(
    @Param('id') id: number,
    @Body() updatePokemonTypeDto: UpdatePokemonTypeDto,
  ): Promise<PokemonType> {
    return this.pokemonTypeService.update(id, updatePokemonTypeDto);
  }

  @Delete(':id')
  delete(@Param('id') id: number): Promise<void> {
    return this.pokemonTypeService.remove(id);
  }
}
