import { ApiProperty } from '@nestjs/swagger';

export class CreatePokemonTypeDto {
  @ApiProperty({ required: true })
  name: string;
}
