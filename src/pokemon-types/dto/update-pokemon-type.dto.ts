import { ApiProperty } from '@nestjs/swagger';

export class UpdatePokemonTypeDto {
  @ApiProperty()
  id: number;

  @ApiProperty({ required: true })
  name: string;
}
