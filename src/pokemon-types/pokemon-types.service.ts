import { PokemonType } from './pokemon-type.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class PokemonTypeService {
  constructor(
    @InjectRepository(PokemonType)
    private pokemonTypeRepository: Repository<PokemonType>,
  ) {}

  async create(data: Object): Promise<PokemonType> {
    return await this.pokemonTypeRepository.save(data);
  }

  async update(id: number, data: Object): Promise<PokemonType> {
    return await this.pokemonTypeRepository.save({ id, ...data });
  }

  findAll(): Promise<PokemonType[]> {
    return this.pokemonTypeRepository.find();
  }

  findOne(id: number): Promise<PokemonType> {
    return this.pokemonTypeRepository.findOne(id);
  }

  async remove(id: number): Promise<void> {
    await this.pokemonTypeRepository.delete(id);
  }
}
