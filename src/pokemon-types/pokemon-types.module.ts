import { BoxModule } from '../boxes/boxes.module';
import { PokemonTypeController } from './pokemon-types.controller';
import { PokemonTypeService } from './pokemon-types.service';
import { PokemonType } from './pokemon-type.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';

@Module({
  imports: [TypeOrmModule.forFeature([PokemonType]), BoxModule],
  providers: [PokemonTypeService],
  controllers: [PokemonTypeController],
  exports: [PokemonTypeService]
})
export class PokemonTypeModule {}
