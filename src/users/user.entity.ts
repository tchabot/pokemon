import { Box } from './../boxes/box.entity';
import {
  PrimaryGeneratedColumn,
  Entity,
  Column,
  BeforeInsert,
  OneToMany,
} from 'typeorm';
import bcrypt from 'bcrypt';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  username: string;

  @Column()
  password: string;

  @OneToMany(
    type => Box,
    box => box.user,
  )
  boxes: Box[];

  @BeforeInsert() async hashPassword() {
    this.password = await bcrypt.hash(this.password, 10);
  }
}
