import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './user.entity';
import { Repository } from 'typeorm';
import { Test } from '@nestjs/testing';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';

describe('UserController', () => {
  let usersController: UsersController;
  let usersService: UsersService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [
        {
          provide: 'UserRepository',
          useClass: Repository,
        },
        UsersService,
      ],
    }).compile();

    usersService = moduleRef.get<UsersService>(UsersService);
    usersController = moduleRef.get<UsersController>(UsersController);
  });

  describe('findAll', () => {
    it('should return an array of users', async () => {
      const result = [];

      jest.spyOn(usersService, 'findAll').mockResolvedValue(result);
      expect(await usersController.findAll()).toBe(result);
    });
  });

  describe('findOne', () => {
    it('should return a user', async () => {
      const result = new User();

      jest.spyOn(usersService, 'findOne').mockResolvedValue(result);
      expect(await usersController.findOne(1)).toBe(result);
    });
  });

  describe('create', () => {
    it('should return a new user', async () => {
      const user = new User();
      const userDto = new CreateUserDto();

      jest.spyOn(usersService, 'create').mockResolvedValue(user);
      expect(await usersController.create(userDto)).toBe(user);
    });
  });

  describe('update', () => {
    it('should return an updated User', async () => {
      const user = new User();
      const updateUserDto = new UpdateUserDto();

      jest.spyOn(usersService, 'update').mockResolvedValue(user);
      expect(await usersController.update(user.id, updateUserDto)).toBe(user);
    });
  });

  describe('delete', () => {
    it('should delete a User', async () => {
      const user = new User();

      jest.spyOn(usersService, 'remove').mockResolvedValue();
      expect(await usersController.delete(user.id)).toBe(undefined);
    });
  });
});
