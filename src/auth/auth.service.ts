import { User } from './../users/user.entity';
import { Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
const bcrypt = require('bcrypt');

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async validateUser(username: string, pass: string): Promise<any> {
    const user = await this.usersService.findByUsername(username);

    if (user) {
      const match = await bcrypt.compare(pass, user.password);

      if (match) {
        const { password, ...result } = user;
        return result;
      }
    }

    return null;
  }

  async login(credentials: User) {
    const user = await this.validateUser(
      credentials.username,
      credentials.password,
    );

    if (user) {
      const payload = { id: user.id, username: user.username, sub: user.id };
      return {
        access_token: this.jwtService.sign(payload),
      };
    }

    return null;
  }
}
