import { Pokemon } from './../pokemons/pokemon.entity';
import { User } from '../users/user.entity';
import {
  PrimaryGeneratedColumn,
  Entity,
  ManyToOne,
  OneToMany,
  Column,
} from 'typeorm';

@Entity()
export class Box {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @ManyToOne(
    type => User,
    user => user.boxes,
  )
  user: User;

  @OneToMany(
    type => Pokemon,
    pokemon => pokemon.box,
  )
  pokemons: Pokemon;
}
