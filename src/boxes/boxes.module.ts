import { UsersModule } from './../users/users.module';
import { BoxController } from './boxes.controller';
import { BoxService } from './boxes.service';
import { Box } from './box.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';

@Module({
  imports: [TypeOrmModule.forFeature([Box]), UsersModule],
  providers: [BoxService],
  controllers: [BoxController],
  exports: [BoxService],
})
export class BoxModule {}
