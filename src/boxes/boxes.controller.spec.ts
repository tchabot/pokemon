import { UsersService } from './../users/users.service';
import { UpdateBoxDto } from './dto/update-box.dto';
import { Box } from './box.entity';
import { CreateBoxDto } from './dto/create-box.dto';
import { BoxService } from './boxes.service';
import { BoxController } from './boxes.controller';
import { Repository } from 'typeorm';
import { Test } from '@nestjs/testing';
const httpMocks = require('node-mocks-http');

describe('BoxController', () => {
  let boxController: BoxController;
  let boxService: BoxService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [BoxController],
      providers: [
        {
          provide: 'BoxRepository',
          useClass: Repository,
        },
        {
          provide: 'UserRepository',
          useClass: Repository,
        },
        BoxService,
        UsersService,
      ],
    }).compile();

    boxService = moduleRef.get<BoxService>(BoxService);
    boxController = moduleRef.get<BoxController>(BoxController);
  });

  describe('findAll', () => {
    it('should return an array of Box', async () => {
      const result = [];
      const req = httpMocks.createRequest();

      jest.spyOn(boxService, 'findAll').mockResolvedValue(result);
      expect(await boxController.findAll(req)).toBe(result);
    });
  });

  describe('findOne', () => {
    it('should return a Box', async () => {
      const result = new Box();

      jest.spyOn(boxService, 'findOne').mockResolvedValue(result);
      expect(await boxController.findOne(1)).toBe(result);
    });
  });

  describe('create', () => {
    it('should return a new Box', async () => {
      const box = new Box();
      const boxDto = new CreateBoxDto();
      const req = httpMocks.createRequest();

      jest.spyOn(boxService, 'create').mockResolvedValue(box);
      expect(await boxController.create(req, boxDto)).toBe(box);
    });
  });

  describe('update', () => {
    it('should return an updated Box', async () => {
      const box = new Box();
      const updateBoxDto = new UpdateBoxDto();

      jest.spyOn(boxService, 'update').mockResolvedValue(box);
      expect(await boxController.update(box.id, updateBoxDto)).toBe(box);
    });
  });
});
