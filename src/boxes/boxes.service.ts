import { UsersService } from './../users/users.service';
import { User } from './../users/user.entity';
import { BoxDeleteException } from './../exceptions/boxes-creation.exception';
import { Box } from './box.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';

@Injectable()
export class BoxService {
  constructor(
    @InjectRepository(Box)
    private boxRepository: Repository<Box>,
    private usersService: UsersService,
  ) {}

  async create(data: any, user: User): Promise<Box> {
    let newBox = { ...data, user: { id: user.id } };
    return await this.boxRepository.save(newBox);
  }

  async update(id: number, data: Object): Promise<Box> {
    return await this.boxRepository.save({ id, ...data });
  }

  async findAll(user: User): Promise<Box[]> {
    if (user.username === 'admin') {
      return this.boxRepository.find({
        relations: ['user', 'pokemons', 'pokemons.pokemonType'],
      });
    } else {
      let dbUser = await this.usersService.findByUsername(user.username);
      return dbUser.boxes;
    }
  }

  findOne(id: number): Promise<Box> {
    return this.boxRepository.findOne(id, {
      relations: ['pokemons', 'pokemons.pokemonType', 'user'],
    });
  }

  async remove(id: number, user: User): Promise<void> {
    let authorizedDeletion = await this.boxCanBeDeleted(id, user.id);

    if (!authorizedDeletion['success']) {
      throw new BoxDeleteException(authorizedDeletion['message']);
    }

    await this.boxRepository.delete(id);
  }

  /**
   * Box can only be deleted by his owner and if there is no pokemon inside
   *
   * @param boxId
   * @param userId
   */
  async boxCanBeDeleted(boxId: number, userId: number): Promise<Object> {
    let box = await this.findOne(boxId);
    let data = {
      success: true,
      message: 'Suppression de la boîte avec succès',
    };

    if (box.user.id != userId) {
      data.success = false;
      data.message =
        'Suppression de la boîte impossible, elle ne vous appartient pas';
      return data;
    }

    if (Object.keys(box.pokemons).length > 0) {
      data.success = false;
      data.message =
        'Suppression de la boîte impossible, elle contient des pokémons';
      return data;
    }

    return data;
  }
}
