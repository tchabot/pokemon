import { JwtAuthGuard } from './../auth/jwt-auth.guard';
import { UpdateBoxDto } from './dto/update-box.dto';
import { CreateBoxDto } from './dto/create-box.dto';
import { Box } from './box.entity';
import { BoxService } from './boxes.service';
import {
  Controller,
  Get,
  Post,
  Delete,
  Body,
  Put,
  Param,
  UseGuards,
  Request,
} from '@nestjs/common';

@UseGuards(JwtAuthGuard)
@Controller('boxes')
export class BoxController {
  constructor(private readonly boxService: BoxService) {}

  @Get()
  findAll(@Request() req: any): Promise<Box[]> {
    return this.boxService.findAll(req.user);
  }

  @Get(':id')
  findOne(@Param('id') id: number): Promise<Box> {
    return this.boxService.findOne(id);
  }

  @Put(':id')
  update(
    @Param('id') id: number,
    @Body() updateBoxDto: UpdateBoxDto,
  ): Promise<Box> {
    return this.boxService.update(id, updateBoxDto);
  }

  @Post()
  create(@Request() req: any, @Body() createBoxDto: CreateBoxDto): Promise<Box> {
    return this.boxService.create(createBoxDto, req.user);
  }

  @Delete(':id')
  delete(@Request() req: any, @Param('id') id: number): Promise<void> {
    return this.boxService.remove(id, req.user);
  }
}
