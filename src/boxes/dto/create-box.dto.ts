import { User } from '../../users/user.entity';
import { ApiProperty } from '@nestjs/swagger';

export class CreateBoxDto {
  @ApiProperty({ required: true })
  user: User;
}
