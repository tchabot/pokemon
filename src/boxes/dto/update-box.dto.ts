import { User } from '../../users/user.entity';
import { ApiProperty } from '@nestjs/swagger';

export class UpdateBoxDto {
  @ApiProperty()
  id: number;

  @ApiProperty({ required: true })
  user: User;
}
