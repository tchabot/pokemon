import { Box } from './../boxes/box.entity';
import { BoxService } from './../boxes/boxes.service';
import { PokemonCreationException } from '../exceptions/pokemon-creation.exception';
import { PokemonTypeService } from '../pokemon-types/pokemon-types.service';
import { InjectRepository } from '@nestjs/typeorm';
import { Pokemon } from './pokemon.entity';
import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';

@Injectable()
export class PokemonService {
  constructor(
    @InjectRepository(Pokemon)
    private pokemonRepository: Repository<Pokemon>,
    private pokemonTypeService: PokemonTypeService,
    private boxService: BoxService,
  ) {}

  async create(data: Object): Promise<Pokemon> {
    let canCreate = await this.validator(data);

    if (canCreate) {
      return await this.pokemonRepository.save(data);
    }
  }

  async update(id: number, data: Object): Promise<Pokemon> {
    let canUpdate = await this.validator(data);

    if (canUpdate) {
      return await this.pokemonRepository.save({ id, ...data });
    }
  }

  findAll(): Promise<Pokemon[]> {
    return this.pokemonRepository.find({ relations: ['box', 'pokemonType'] });
  }

  findOne(id: number): Promise<Pokemon> {
    return this.pokemonRepository.findOne(id, { relations: ['box', 'pokemonType'] });
  }

  async remove(id: number): Promise<void> {
    await this.pokemonRepository.delete(id);
  }

  /**
   * Check maximum pokemon types and number
   */
  async validator(data: any): Promise<boolean | void> {
    let box: Box = await this.boxService.findOne(data.box.id);
    let pokemons: any = box.pokemons;

    if (pokemons.length > 23) {
      throw new PokemonCreationException(
        'Cette boîte ne peut pas contenir davantage de pokémons',
      );
    }

    let types = pokemons.map(pokemon => {
      return pokemon.pokemonType.id;
    });

    let setTypes = new Set(types);

    /**
     * If this is a new pokemon type, we check if there is'nt already two different
     */
    if (!setTypes.has(data.pokemonType.id) && setTypes.size > 1) {
      throw new PokemonCreationException(
        'Cette boîte contient déjà deux types différents de pokémons',
      );
    }

    return true;
  }
}
