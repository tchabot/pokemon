import { JwtAuthGuard } from './../auth/jwt-auth.guard';
import { UpdatePokemonDto } from './dto/update-pokemon.dto';
import { createPokemonDto } from './dto/create-pokemon.dto';
import { Pokemon } from './pokemon.entity';
import { PokemonService } from './pokemons.service';
import {
  Controller,
  Get,
  Post,
  Delete,
  Body,
  Put,
  Param,
  UseGuards,
} from '@nestjs/common';

@UseGuards(JwtAuthGuard)
@Controller('pokemons')
export class PokemonController {
  constructor(private readonly pokemonService: PokemonService) {}

  @Get()
  findAll(): Promise<Pokemon[]> {
    return this.pokemonService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: number): Promise<Pokemon> {
    return this.pokemonService.findOne(id);
  }

  @Put(':id')
  update(
    @Param('id') id: number,
    @Body() updatePokemonDto: UpdatePokemonDto,
  ): Promise<Pokemon> {
    return this.pokemonService.update(id, updatePokemonDto);
  }

  @Post()
  create(@Body() createPokemonDto: createPokemonDto): Promise<Pokemon> {
    return this.pokemonService.create(createPokemonDto);
  }

  @Delete(':id')
  delete(@Param('id') id: number): Promise<void> {
    return this.pokemonService.remove(id);
  }
}
