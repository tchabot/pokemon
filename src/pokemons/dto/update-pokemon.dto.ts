import { PokemonType } from './../../pokemon-types/pokemon-type.entity';
import { Box } from './../../boxes/box.entity';
import { ApiProperty } from "@nestjs/swagger";

export class UpdatePokemonDto {
    @ApiProperty()
    id: number

    @ApiProperty()
    name: string

    @ApiProperty()
    box: Box

    @ApiProperty()
    pokemonType: PokemonType
}