import { PokemonType } from './../../pokemon-types/pokemon-type.entity';
import { Box } from './../../boxes/box.entity';
import { ApiProperty } from "@nestjs/swagger";

export class createPokemonDto {
    @ApiProperty()
    name: string

    @ApiProperty()
    box: Box

    @ApiProperty()
    pokemonType: PokemonType
}