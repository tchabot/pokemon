import { PokemonType } from './../pokemon-types/pokemon-type.entity';
import { Box } from './../boxes/box.entity';
import { PrimaryGeneratedColumn, Entity, Column, ManyToOne } from 'typeorm';

@Entity()
export class Pokemon {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @ManyToOne(
    type => Box,
    box => box.pokemons,
  )
  box: Box;

  @ManyToOne(
    type => PokemonType,
    pokemonType => pokemonType.pokemons,
  )
  pokemonType: PokemonType;
}
