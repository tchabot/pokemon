import { BoxModule } from './../boxes/boxes.module';
import { PokemonTypeModule } from '../pokemon-types/pokemon-types.module';
import { PokemonController } from './pokemons.controller';
import { PokemonService } from './pokemons.service';
import { Pokemon } from './pokemon.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';

@Module({
  imports: [TypeOrmModule.forFeature([Pokemon]), PokemonTypeModule, BoxModule],
  providers: [PokemonService],
  controllers: [PokemonController],
  exports: [PokemonService]
})
export class PokemonModule {}