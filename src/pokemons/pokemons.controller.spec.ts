import { UsersService } from './../users/users.service';
import { BoxService } from './../boxes/boxes.service';
import { UpdatePokemonDto } from './dto/update-pokemon.dto';
import { PokemonTypeService } from '../pokemon-types/pokemon-types.service';
import { createPokemonDto } from './dto/create-pokemon.dto';
import { Pokemon } from './pokemon.entity';
import { PokemonService } from './pokemons.service';
import { PokemonController } from './pokemons.controller';
import { Repository } from 'typeorm';
import { Test } from '@nestjs/testing';

describe('PokemonController', () => {
  let pokemonController: PokemonController;
  let pokemonService: PokemonService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [PokemonController],
      providers: [
        {
          provide: 'PokemonRepository',
          useClass: Repository,
        },
        {
          provide: 'PokemonTypeRepository',
          useClass: Repository,
        },
        {
          provide: 'BoxRepository',
          useClass: Repository,
        },
        {
          provide: 'UserRepository',
          useClass: Repository,
        },
        PokemonService,
        PokemonTypeService,
        BoxService,
        UsersService
      ],
    }).compile();

    pokemonService = moduleRef.get<PokemonService>(PokemonService);
    pokemonController = moduleRef.get<PokemonController>(PokemonController);
  });

  describe('findAll', () => {
    it('should return an array of Pokemons', async () => {
      const result = [];

      jest.spyOn(pokemonService, 'findAll').mockResolvedValue(result);
      expect(await pokemonController.findAll()).toBe(result);
    });
  });

  describe('findOne', () => {
    it('should return a Pokemon', async () => {
      const result = new Pokemon();

      jest.spyOn(pokemonService, 'findOne').mockResolvedValue(result);
      expect(await pokemonController.findOne(1)).toBe(result);
    });
  });

  describe('create', () => {
    it('should return a new Pokemon', async () => {
      const pokemon = new Pokemon();
      const pokemonDto = new createPokemonDto();

      jest.spyOn(pokemonService, 'create').mockResolvedValue(pokemon);
      expect(await pokemonController.create(pokemonDto)).toBe(pokemon);
    });
  });

  describe('update', () => {
    it('should return an updated Pokemon', async () => {
      const pokemon = new Pokemon();
      const updatePokemonDto = new UpdatePokemonDto();

      jest.spyOn(pokemonService, 'update').mockResolvedValue(pokemon);
      expect(await pokemonController.update(pokemon.id, updatePokemonDto)).toBe(
        pokemon,
      );
    });
  });

  describe('delete', () => {
    it('should delete a Pokemon', async () => {
      const pokemon = new Pokemon();

      jest.spyOn(pokemonService, 'remove').mockResolvedValue();
      expect(await pokemonController.delete(pokemon.id)).toBe(undefined);
    });
  });
});
