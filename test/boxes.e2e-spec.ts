import { BoxController } from './../src/boxes/boxes.controller';
import { BoxService } from '../src/boxes/boxes.service';
import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';

describe('Boxes', () => {
  let app: INestApplication;
  let boxService = { findAll: () => ['test'] };

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [BoxService],  
      controllers: [BoxController],
    })
      .overrideProvider(BoxService)
      .useValue(boxService)
      .compile();

    app = moduleRef.createNestApplication();
    await app.init();
  });

  it(`/GET boxes`, () => {
    return request(app.getHttpServer())
      .get('/boxes')
      .expect(200)
      .expect(boxService.findAll());
  });

  afterAll(async () => {
    await app.close();
  });
});
