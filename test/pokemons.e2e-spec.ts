import { PokemonController } from './../src/pokemons/pokemons.controller';
import { PokemonService } from '../src/pokemons/pokemons.service';
import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';

describe('Pokemons', () => {
  let app: INestApplication;
  let pokemonService = { findAll: () => ['test'] };

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [PokemonService],
      controllers: [PokemonController],
    })
      .overrideProvider(PokemonService)
      .useValue(pokemonService)
      .compile();

    app = moduleRef.createNestApplication();
    await app.init();
  });

  it(`/GET pokemons`, () => {
    return request(app.getHttpServer())
      .get('/pokemons')
      .expect(200)
      .expect(pokemonService.findAll());
  });

  afterAll(async () => {
    await app.close();
  });
});
