import { PokemonTypeController } from './../src/pokemon-types/pokemon-types.controller';
import { PokemonTypeService } from '../src/pokemon-types/pokemon-types.service';
import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';

describe('PokemonTypes', () => {
  let app: INestApplication;
  let pokemonTypeService = { findAll: () => ['test'] };

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [PokemonTypeService],
      controllers: [PokemonTypeController],
    })
      .overrideProvider(PokemonTypeService)
      .useValue(pokemonTypeService)
      .compile();

    app = moduleRef.createNestApplication();
    await app.init();
  });

  it(`/GET pokemon-types`, () => {
    return request(app.getHttpServer())
      .get('/pokemon-types')
      .expect(200)
      .expect(pokemonTypeService.findAll());
  });

  afterAll(async () => {
    await app.close();
  });
});
